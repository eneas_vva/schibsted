<?php

$messages["user_not_exists"] =		[ 	"en" => "user doesn't exist",
										"es" => "el usuario no existe"
									];
$messages["user_created"] =			[ 	"en" => "user %d with username %s created.",
										"es" => "usuario %d con username %s created."
									];
$messages["user_creating_error"] =  [	"en" => "it wasn't possible to create the user.",
										"es" => "no fué posible crear el usuario.",
									];
$messages["user_deleted"] =			[	"en" => "user %d with username %s deleted.",
										"es" => "el usuario %d con username %s fué eliminado",
									];
$messages["user_updated"] =			[	"en" => "user %d with username %s was updated.",
										"es" => "el usuario %d con username %s fué actualizado",
									];
$messages["user_updating_error"] =	[	"en" => "it wasn't possible to update the user.",
										"es" => "no fué posible actualizar el usuario."
									];
$messages["login_invalid_auth"] =	[	"en" => "invalid login authentication.",
										"es" => "autenticación de login inválida."
									];
$messages["login_invalid_gateway"]= [	"en" => "this page just can be accessed from the proper gateway",
										"es" => "esta página solo puede ser accedida mediante el punto de acceso "
];
$messages["login_not_logged"] =		[	"en" => "you aren't logged.",
										"es" => "no estas logueado."
									];
$messages["login_logged_as"] =		[	"en" => "You are logged in as",
										"es" => "Estas logueado como"
									];
$messages["login_admin_required"] =	[	"en" => "area restricted for only admins.",
										"es" => "area restringida, solo administradores."
									];
$messages["http_not_found"] =		[	"en" => "the page you're trying to access not found.",
										"es" => "no existe la página a la que intentas acceder."
									];
$messages["http_method_disallowed"]=[	"en" => "we can't found any response for your method request.",
										"es" => "no podemos encontrar ninguna respuesta para el método que requiere."
									];
$messages["login_insufficient"]	=	[   "en" => "you doesn't have permission to view this.",
										"es" => "no tienes permisos para ver esta página."
									];
$messages["content_welcome"] =		[   "en" => "Hello",
										"es" => "Hola"
									];
$messages["content_title"] =		[	"en" => "Private page",
										"es" => "Página privada"
									];