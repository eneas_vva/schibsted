<?php
use phpunit\framework\TestCase;
use Eneas\Db\DBConnection;
use Eneas\Dao\UserDAO;
use Eneas\Model\UserEntity;
use Eneas\Model\RoleEntity;
use Eneas\Controller\Controller;
use Eneas\Dao\UsersRolesDAO;
/**
 * Isolation Between Database Connections
 If the same database is being read and written using two different database connections
 (two different sqlite3 objects returned by separate calls to sqlite3_open()) and the two
 database connections do not have a shared cache, then the reader is only able to see complete
 committed transactions from the writer. Partial changes by the writer that have not been
 committed are invisible to the reader. This is true regardless of whether the two database
 connections are in the same thread, in different threads of the same process, or in different
 processes. This is the usual and expected behavior for SQL database systems.
 	
 Solución: Revertir los cambios manualmente
 */

class RestTest extends TestCase {
	protected static $dsn;
	protected function setUp()
	{
		$this->client = new GuzzleHttp\Client([
				'base_uri' => 'http://localhost:8000'
		]);
		self::$dsn = DBConnection::getInstance();
	}
	

	public function testDelete_DeleteUser_UserObject()
	{
		$user = new UserEntity();
		$password = "my_test_password";
		$user->setUsername("user_".rand(0, 1000));
		$user->setPasswd($password);
		$user->getRoles()->addRole((new RoleEntity())->setId(1));
		$user->getRoles()->addRole((new RoleEntity())->setId(3));
		$userDAO = new UserDAO();
		$this->assertTrue($userDAO->create($user), "user couldn't be created");
		
		$response = $this->client->request('DELETE', sprintf('/users/%d', $user->getId()), [
				'form_params' => [
						'username'  => $user->getUsername()
						],
						'auth' => ['root', 'root']
				]);
		$this->assertEquals(200, $response->getStatusCode());
	
		$user_deleted = new UserEntity();
		$user_deleted->setUsername($user->getUsername());
		$userDAO = new UserDAO();
		$this->assertFalse($userDAO->getByUserName($user_deleted));
	
	}
	
	public function testPost_NewUser_UserObject()
	{
		$user = new UserEntity();
		$password = "my_test_password";
		$user->setUsername("user_".rand(0, 1000));
		$user->setPasswd($password);
		$user->getRoles()->addRole((new RoleEntity())->setId(1));
		$user->getRoles()->addRole((new RoleEntity())->setId(3));
		
		$response = $this->client->post('/users', [
				'form_params' => [
						'username'  => $user->getUsername(),
						'password'  => $password,
						'roles'	=> 	array_map(function(RoleEntity $r){ return $r->getId(); }, $user->getRoles()->getAll()),
				],
				'auth' => ['root', 'root']
		]);
	
		$this->assertEquals(201, $response->getStatusCode());

		$user_persisted = new UserEntity();
		$user_persisted->setUsername($user->getUsername());
		$userDAO = new UserDAO();
		$userDAO->getByUserName($user_persisted);

		$this->assertNotNull($user_persisted->getId());
		
		// Password is not saved in volatile memory by default for paranoid security issues
// 		$this->assertNotEquals($password, $user_persisted->getPasswd()); // Password must be hashed
// 		$this->assertEquals($user->getPasswd(), $user_persisted->getPasswd());
		$this->assertTrue($user->getRoles()->hasRole($user_persisted->getRoles()->getAll()[0]));
		$this->assertTrue($user->getRoles()->hasRole($user_persisted->getRoles()->getAll()[1]));
		
		$this->assertTrue($userDAO->delete($user_persisted));
	}
	
	public function testPut_UpdateUser_UserObject()
	{
		$user = new UserEntity();
		$password = "my_test_password";
		$user->setUsername("user_".rand(0, 1000));
		$user->setPasswd($password);
		$user->getRoles()->addRole((new RoleEntity())->setId(1));
		$user->getRoles()->addRole((new RoleEntity())->setId(3));
		$userDAO = new UserDAO();
		$this->assertTrue($userDAO->create($user), "user couldn't be created");
		
		$response = $this->client->request('PUT', sprintf('/users/%d', $user->getId()), [
				'form_params' => [
						'username'  => 'modified_'.$user->getUsername(),
						'password'  => $password,
						'roles'		=> [1],
						],
						'auth' => ['root', 'root']
				]);
		
		$this->assertEquals(200, $response->getStatusCode());
		$user_persisted = new UserEntity();
		$user_persisted->setId($user->getId());
		
		$userDAO->getById($user_persisted);
	
		$this->assertNotNull($user_persisted->getId());
	
		$this->assertEquals('modified_'.$user->getUsername(), $user_persisted->getUsername());
		$this->assertTrue($user_persisted->getRoles()->hasRole($user->getRoles()->getAll()[0]));
		$this->assertFalse($user_persisted->getRoles()->hasRole($user->getRoles()->getAll()[1]));
		$this->assertTrue($userDAO->delete($user_persisted));
		
	}
	
	public function testPut_invalid_resource_Http_response() {
		try {
			$response = $this->client->request('PUT', '/users/9999999', [
					'auth' => ['root', 'root']
			]);
		} catch (GuzzleHttp\Exception\ClientException $e) { 
			$this->assertEquals(404, $e->getResponse()->getStatusCode());
		};

		try {
		$response = $this->client->request('DELETE', '/users/9999999', [
				'auth' => ['root', 'root']
		]);
		} catch (GuzzleHttp\Exception\ClientException $e) { 
			$this->assertEquals(404, $e->getResponse()->getStatusCode());
		};
		
		try {
		$response = $this->client->request('GET', '/users/9999999', [
				'auth' => ['root', 'root']
		]);
		} catch (GuzzleHttp\Exception\ClientException $e) { 
			$this->assertEquals(404, $e->getResponse()->getStatusCode());
		};
		
	}
	
}