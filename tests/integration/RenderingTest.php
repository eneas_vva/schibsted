<?php

use \PHPUnit\Framework\TestCase;
use \Eneas\Model\UserEntity;
use \Eneas\Model\RoleEntity;
use \Eneas\Controller\Controller;
use Eneas\Dao\UserDAO;

class RenderingTest extends TestCase {
	protected static $dsn;
	protected $user;
	protected $password;
	
	protected function setUp()
	{

		/**
		 * PHPUnit manda sus cabeceras para su propia sesion,
		 * esto entra en conflicto con la propia sesion que estamos
		 * abriendo para el login, por tanto, desoimos las advertencias
		 * que no son motivadas por errores del código sino del test.
		 */
		PHPUnit_Framework_Error_Warning::$enabled = FALSE;
		error_reporting(E_ALL ^ E_WARNING);
		
		
		$dsn = Eneas\Db\DBConnection::getInstance();
	}
	/**
	 * @before
	 */
	public function setupTestPrivPages() {
		// Creamos el usuario con los privilegios para la página 1
		$this->user = new UserEntity();
		$this->password = "my_test_password";
		$this->user->setUsername("user_".rand(0, 1000));
		$this->user->setPasswd($this->password);


		$this->client = new GuzzleHttp\Client([
				'base_uri' => 'http://localhost:8000',
				'cookies' => true
		]);
		
	}
	
	/**
	 * @after
	 */
	public function tearDownTestPrivPages() {
		$res = $this->client->request('GET', '/logout');
		$userDAO = new UserDAO();
		$userDAO->delete($this->user);
		$this->client = null;
	
	}
	public function testPrivPage1() {
		$this->user->getRoles()->addRole((new RoleEntity())->setId(1)); // PRIV_PAGE 1
		$userDAO = new UserDAO();
		$userDAO->create($this->user);
		

		$res = $this->client->post('/login/', [
				'form_params' => [
						'username' => $this->user->getUsername(),
						'password' => $this->password
				]
		]);
		
		$res = $this->client->request('GET', '/page/1');
		$this->assertEquals(200, $res->getStatusCode());
		
		try {
			$res = $this->client->request('GET', '/page/2');
		} catch (GuzzleHttp\Exception\ClientException $e) { 
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
		
		try {
			$res = $this->client->request('GET', '/page/3');
		} catch (GuzzleHttp\Exception\ClientException $e) { 
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
		
	}

	
	public function testPrivPage2() {
	
		$this->user->getRoles()->addRole((new RoleEntity())->setId(2)); // PRIV_PAGE 2
		$userDAO = new UserDAO();
		$userDAO->create($this->user);
		
		
		$res = $this->client->post('/login/', [
				'form_params' => [
						'username' => $this->user->getUsername(),
						'password' => $this->password
				]
		]);
	
		try {
			$res = $this->client->request('GET', '/page/1');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
	

		$res = $this->client->request('GET', '/page/2');
		$this->assertEquals(200, $res->getStatusCode());
		
		try {
			$res = $this->client->request('GET', '/page/3');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
	
	}
	
	public function testPrivPage3() {
	
		$this->user->getRoles()->addRole((new RoleEntity())->setId(3)); // PRIV_PAGE 3
		$userDAO = new UserDAO();
		$userDAO->create($this->user);
	
	
		$res = $this->client->post('/login/', [
				'form_params' => [
						'username' => $this->user->getUsername(),
						'password' => $this->password
				]
		]);
	
		try {
			$res = $this->client->request('GET', '/page/1');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
	
		try {
			$res = $this->client->request('GET', '/page/2');
		} catch (GuzzleHttp\Exception\ClientException $e) {
			$this->assertEquals(403, $e->getResponse()->getStatusCode());
		};
		
		$res = $this->client->request('GET', '/page/3');
		$this->assertEquals(200, $res->getStatusCode());
	
	}
	
	public function testAdminsCanAccessAnyPage() {
	
		$this->user->getRoles()->addRole((new RoleEntity())->setId(4)); // ADMIN ROLE
		$userDAO = new UserDAO();
		$userDAO->create($this->user);
	
	
		$res = $this->client->post('/login/', [
				'form_params' => [
						'username' => $this->user->getUsername(),
						'password' => $this->password
				]
		]);
	

		$res = $this->client->request('GET', '/page/1');
		$this->assertEquals(200, $res->getStatusCode());
	
		$res = $this->client->request('GET', '/page/2');
		$this->assertEquals(200, $res->getStatusCode());
	
		$res = $this->client->request('GET', '/page/3');
		$this->assertEquals(200, $res->getStatusCode());
	
	}
	
	public function tearDown() {
		PHPUnit_Framework_Error_Warning::$enabled = TRUE;
		error_reporting(E_ALL);
	}
}