<?php
use PHPUnit\Framework\TestCase;
use Eneas\Controller\Controller;
use Eneas\Dao\UserDAO;
use Eneas\Model\UserEntity;

class LoginTest extends TestCase {
	/** @var $dsn PDO */
	private static $dsn;
	public function setUp() {
		/**
		 * PHPUnit manda sus cabeceras para su propia sesion,
		 * esto entra en conflicto con la propia sesion que estamos
		 * abriendo para el login, por tanto, desoimos las advertencias
		 * que no son motivadas por errores del código sino del test.
		 */
		PHPUnit_Framework_Error_Warning::$enabled = FALSE;
		error_reporting(E_ALL ^ E_WARNING);
		$dsn = Eneas\Db\DBConnection::getInstance();
		
	}
	public function testLogin() {
		Controller::login_logic("root", "root");
		$this->assertTrue(Controller::login_is_logged());
	}
	
	public function testCheckContextParams() {
		Controller::login_logic("root", "root");
		$userFromContext = Controller::getUserContext();
		$userDAO = new UserDAO();
		$user = new UserEntity();
		$user->setUsername("root");
		$userDAO->getByUserName($user);
	
		$this->assertEquals($user->getUsername(), $userFromContext->getUsername());
		$this->assertNull($userFromContext->getPasswd());// La contraseña no está en sesión
		$this->assertEquals($user->getRoles(), $userFromContext->getRoles());
		
	}
	
	public function testLoginRoles() {
		Controller::login_logic("root", "root");
		$this->assertTrue(Controller::login_hasRoleByTag('ADMIN'));
	}
	public function testDestroySessionForInactivity() {
		Controller::login_logic("root", "root");
		$this->assertTrue(Controller::login_is_logged());
		Controller::$SESSION_LIFETIME = 1;
		Controller::check_activity();
		sleep(2);
		Controller::check_activity();
		$this->assertFalse(Controller::login_is_logged());
		
		
	}
	
	public function tearDown() {
		PHPUnit_Framework_Error_Warning::$enabled = TRUE;
		error_reporting(E_ALL);
	}
}