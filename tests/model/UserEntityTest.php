<?php

use Eneas\Model\UserEntity;
use Eneas\Model\UserRolesEntity;


use PHPUnit\Framework\TestCase;
use Eneas\Model\RoleEntity;

class UserEntityTest extends TestCase {

	
	public function testPasswordOnlyWritable() {
		/** @var UserEntity */
		$userEntity = new UserEntity();
		$password = "this_is_a_password";
		$userEntity->setPasswd($password);
		
		$this->assertNotEquals($userEntity->getPasswd(), $password);
	}
	
	public function testUserRegistration() {
		/** @var UserEntity */
		$userEntity = new UserEntity();
		$userEntity->setUsername("Alejandro");
		$userEntity->setPasswd("Passwd");
		$roles = $userEntity->getRoles();
		$roles->addRole((new RoleEntity())->setId(0)->setName("ADMIN"));
		$roles->addRole((new RoleEntity())->setId(1)->setName("ROLE1"));
		
		/** @var UserEntity */
		$userEntity2 = new UserEntity();
		$userEntity2->setUsername("Alejandro");
		$userEntity2->setPasswd("Passwd");
		$roles2 = $userEntity2->getRoles();
		$roles2->addRole((new RoleEntity())->setId(0)->setName("ADMIN"));
		$roles2->addRole((new RoleEntity())->setId(1)->setName("ROLE1"));
		
		$this->assertEquals($userEntity, $userEntity2);
	}
}