<?php
use Eneas\Model\RoleEntity;
use PHPUnit\Framework\TestCase;
use Eneas\Model\UserRolesEntity;

class UserRolesEntityTest extends TestCase {
	protected $userRolesEntity;
	public function setUp() {
		$this->userRolesEntity = new UserRolesEntity();
		$this->userRolesEntity->addRole((new RoleEntity())->setId(1)->setName("ROLE2"));
	}
	
	public function testPrivileges() {
		/** @var $userRolesEntity UserRolesEntity */
		$this->assertTrue($this->userRolesEntity->hasRole((new RoleEntity())->setId(1)->setName("ROLE2")));
		$this->assertFalse($this->userRolesEntity->hasRole((new RoleEntity())->setId(0)->setName("ROLE1")));
	}
	
	public function testUserRoles() {
		$roles2 = new UserRolesEntity();
		$roles2->addRole((new RoleEntity())->setId(0)->setName("ADMIN"));
		$roles2->addRole((new RoleEntity())->setId(1)->setName("ROLE1"));
	}
	public function testAddDelRoles() {
		/** @var $roles2 UserRolesEntity */
		$roles2 = new UserRolesEntity();
		$roles2->addRole((new RoleEntity())->setId(0));
		$roles2->addRole((new RoleEntity())->setId(1));
		$roles2->delRole((new RoleEntity())->setId(1));
		
		$this->assertEquals(count($roles2->getAll()), 1);
	}
	
	public function testDuplicates() {
		/** @var $roles2 UserRolesEntity */
		$roles2 = new UserRolesEntity();
		$roles2->addRole((new RoleEntity())->setId(1));
		$roles2->addRole((new RoleEntity())->setId(1));
		
		$this->assertEquals(count($roles2->getAll()), 1);
		
	}
	
}