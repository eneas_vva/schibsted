<?php
use Eneas\Model\RoleEntity;
use PHPUnit\Framework\TestCase;

class RoleEntityTest extends TestCase {
	
	public function testRole() {
		$role1 = (new RoleEntity())->setId(0)->setName("ROLE1");
		$role2 = (new RoleEntity())->setId(1)->setName("ROLE1");
		
		$this->assertNotEquals($role1, $role2);
		$this->assertNotEquals($role1->getId(), $role2->getId());
		$this->assertEquals($role1->getName(), $role2->getName());
	}
}
