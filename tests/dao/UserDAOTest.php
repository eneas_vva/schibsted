<?php

use phpunit\framework\TestCase;
use Eneas\Db\DBConnection;
use Eneas\Dao\UserDAO;
use Eneas\Model\UserEntity;
use Eneas\Model\RoleEntity;

class UserDAOTest extends TestCase {
	/** @var $dsn PDO  */
	protected static $dsn;
	public static function setUpBeforeClass() {
		self::$dsn = DBConnection::getInstanceTesting();
	}
	
	/**
	 * @before
	 */
	public function setupTestNewUser() {
		self::$dsn->beginTransaction();
	}
	public function testNewUser() {
		
		$user = new UserEntity();
		$username = "user_".rand(0, 100000);
		$user->setUsername($username);
 		$user->setPasswd("testingpassword");
// 		$roles = $user->getRoles();
// 		$roles->addRole((new RoleEntity())->setId(0)->setName("ADMIN"));
// 		$roles->addRole((new RoleEntity())->setId(1)->setName("ROLE1"));
		
		$user_copy = clone $user;
		$userDAO = new UserDAO(self::$dsn);
		$userDAO->create($user);
		
		$this->assertNotEquals($user, $user_copy);
		$user_copy->setId($user->getId());
		
		$this->assertEquals($user, $user_copy);
		
	}
	/**
	 * @after
	 */
	public function teardownTestNewUser() {
		self::$dsn->rollBack();
		
	}
	
	
	 public static function tearDownAfterClass() {
		self::$dsn = null;
	}
}