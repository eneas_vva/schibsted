<?php

use phpunit\framework\TestCase;
use Eneas\Db\DBConnection;
use Eneas\Dao\UserDAO;
use Eneas\Model\UserEntity;
use Eneas\Model\RoleEntity;
use Eneas\Model\UserRolesEntity;
use Eneas\Dao\UsersRolesDAO;
use Eneas\Dao\RoleDAO;

class UsersRolesDAOTest extends TestCase {
	/** @var $dsn PDO  */
	protected static $dsn;
	public static function setUpBeforeClass() {
		self::$dsn = DBConnection::getInstanceTesting();
	}

	public function testCheckAdminRole() {
		$username = new UserEntity();
		$username->setUsername('root');
		$userDao = new UserDAO(self::$dsn);
		$userDao->getByUserName($username);
	
		$rolesDao = new RoleDAO(self::$dsn);
		$role = new RoleEntity();
		$role->setName('ADMIN');
		$rolesDao->getByTag($role);
		$this->assertTrue($username->getRoles()->hasRole($role));
		
	}
	
	
	public static function tearDownAfterClass() {
		self::$dsn = null;
	}
}