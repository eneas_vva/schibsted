<?php namespace Eneas\Model;
use Eneas\Model\RoleEntity;

class UserRolesEntity {
	/** @var array */
	private $roles;
	
	function __construct() {
		$this->roles = array();
	}
	
	public function getAll() {
		return $this->roles;
	}
	
	public function hasRole(RoleEntity $role) {
		return (bool)array_filter($this->roles, function($_role) use($role) { return $_role->getId() == $role->getId(); });
	}
	
	public function addRole(RoleEntity $role) {
		if(!$this->hasRole($role))
			array_push($this->roles, $role);
	}
	
	/**
	 * Busco el role en la lista y si está lo elimino
	 * @param unknown $role
	 */
	public function delRole(RoleEntity $role) {
		foreach ($this->roles as $key => $value) {
			if($role->getId() == $value->getId())
				unset($this->roles[$key]);
		}
	}
	
	
	public function __toString() {
		$role_names = array_map(function(\Eneas\Model\RoleEntity $role){ return $role->getName(); }, $this->roles);
		return join(", ", $role_names);
	}
}