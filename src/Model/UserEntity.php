<?php namespace Eneas\Model;

/**
 * @author Eneas
 *
 */
class UserEntity {
	private $id;
	private $username;
	private $passwd;
	
	/** @var \UserRolesEntity */
	private $roles;
	
	function __construct() {
		$this->roles = new UserRolesEntity();
	}
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getUsername() {
		return $this->username;
	}
	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}
	
	public function getPasswd() {
		return $this->passwd;
	}
	
	/**
	 * Pone la password a null
	 * @param string $passwd
	 * @return \Model\User\User
	 */
	public function setNullPasswd() {
		$this->passwd = null;
		return $this;
	}
	
	/**
	 * La password es read-only por algoritmo MD5 
	 * @param string $passwd
	 * @return \Model\User\User
	 */
	public function setPasswd($passwd) {
		$this->passwd = md5($passwd);
		return $this;
	}
	
	/**
	 * 
	 * @return UserRolesEntity
	 */
	public function getRoles() {
		return $this->roles;
	}
	
	/**
	 * 
	 * @param unknown $roles
	 * @return \Eneas\Model\UserEntity
	 */
	public function setRoles($roles) {
		$this->roles = $roles;
		return $this;
	}
	

	
	
	
	
}