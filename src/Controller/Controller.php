<?php 
namespace Eneas\Controller;

use \Eneas\Model\UserEntity;
use \Eneas\Dao\UserDAO;
use Eneas\Model\UserRolesEntity;
use Eneas\Model\RoleEntity;
use Eneas\Dao\RoleDAO;

class Controller {
	public static $SESSION_LIFETIME = 300; // 5 minutes
	
	/**
	 * Renderiza página de índice
	 */
	static function get_index() {
		require_once $_SERVER['DOCUMENT_ROOT'] ."/view/index.html";
	}
	
	/**
	 * Renderiza la lista de usuarios
	 */
	static function users_list() {
		require_once $_SERVER['DOCUMENT_ROOT'] .'/view/users/list.php';
	}
	
	/**
	 * Renderiza formulario de alta de usuarios
	 */
	static function users_new_form() {
		require_once $_SERVER['DOCUMENT_ROOT'] .'/view/users/new.php';
	}

	/**
	 * Renderiza la página de login
	 * 
	 * @param array $vars Si contiene el elemento 'id' será reenviado a dicha página tras el login
	 */
	static function login_form($vars) {
		if(!self::login_is_logged())
			require_once $_SERVER['DOCUMENT_ROOT'].'/view/login/login.php';
		else self::login_profile();
	}
	
	/**
	 * Renderiza la página de perfil del usuario autentificado
	 */
	static function login_profile() {
		require_once $_SERVER['DOCUMENT_ROOT'] .'/view/login/profile.php';
	}
	
	/**
	 * Renderiza la página en función del identificador
	 * @param array $vars Contiene el elemento 'id' con el identificador de la página
	 * 
	 * @precondition $vars['id'] debe existir, no importa que el identificador sea inválido, 
	 * 					en caso de no existir, muestra error descriptivo con estado 404
	 * @poscondition Renderiza la página solicitada
	 */
	static function get_page($vars) {
		$valid_pages = [1, 2, 3];
		$page = $vars['id'];
		if(!in_array($page, $valid_pages)) { // Si está en el array de páginas registradas
			http_response_code(404);
			print self::get_message("http_not_found");
		}
		else {
			if(!self::login_is_logged()) { // Comprobamos que este registrado
				header('Location: /login/'.$page); // Le llevamos a la página de login
				exit;
			}
			else if(!self::login_hasRoleByTag('PAGE_'.$page) && !self::login_hasRoleByTag('ADMIN')) { // Si está logueado, comprobamos su role
				http_response_code(403);
				print self::get_message("login_insufficient");
				exit;
			}
			$has_permission = true;
			require_once $_SERVER['DOCUMENT_ROOT'] .sprintf('/view/pages/priv_page%d.php', $page);
		}
	}
	
	/**
	 * Renderiza página de actualización de un usuario dado
	 * @param array $vars Debe contener el elemento id con la descripción del usuario
	 */
	static function users_update_form($vars) {
		/* @var UserEntity */
		$user = new UserEntity();
		$user->setId($vars['id']);
		/* @var UserDAO */
		$userDAO = new UserDAO();
	
		$userDAO->getById($user);
	
		require_once $_SERVER['DOCUMENT_ROOT'] .'/view/users/update.php';
	}
	
	/**
	 * Registra en sesión un timestamp de la actividad y comprueba que desde ese último registro hasta
	 * el momento en el que se llama a la función han pasado self::SESSION_LIFETIME segundos
	 * 
	 * @precondition self::SESSION_LIFETIME debe estar definida
	 * @poscondition La sesión será destruida
	 */
	static function check_activity() {
		if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > self::$SESSION_LIFETIME)) {
			session_unset();     // unset $_SESSION variable for the run-time
			session_destroy();   // destroy session data in storage
		}
		$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
	}
	
	/**
	 * Envía la petición HTTP AUTH a la cabecera, para que el navegador solicite usuario y contraseña
	 * @poscondition Se abrirá una sesión de login con los parámetros solicitados o se mandará
	 */
	static function send_http_auth_request() {
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			header('WWW-Authenticate: Basic realm="You must logged in to use User REST API"');
			header('HTTP/1.0 401 Unauthorized');
			print self::get_message("login_invalid_auth");
			exit;
		} else {
			self::login_logic($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
		}
	}
	
	/** 
	 * Muestra la información de un usuario concreto
	 * 
	 * @precondition El usuario debe existir, de lo contrario, mostrará error descriptivo y estado 404
	 * @param  array $vars Debe contener el elemento 'id' con el identificador del usuario
	 */
	static function users_request($vars) {
		/* @var UserEntity */
		$user = new UserEntity();
		$user->setId($vars['id']);
		/* @var UserDAO */
		$userDAO = new UserDAO();
	
		if($userDAO->getById($user) !== false)
			require_once $_SERVER['DOCUMENT_ROOT'] .'/view/users/view.php';
		else {
			http_response_code(404);
			print self::get_message("user_not_exists");
		}
	}
	

	/**
	 * Se crea un usuario nuevo en la base de datos.
	 * 
	 * @precondition $_POST debe contener los parámetros requeridos para el registro (username,password y roles),
	 * 					en caso de error se muestra mensaje descriptivo con estado 500
	 * @poscondition Si el usuario se registró correctamente, se muestra mensaje descriptivo con código de estado 201
	 * 					
	 * @return bool true si el usuario se creo y false en caso contrario.
	 */
	static function users_new() {
		/* @var UserEntity */
		$user = new UserEntity();
		/* @var UserDAO */
		$userDAO = new UserDAO();
	
		$user->setUsername($_POST['username'])->setPasswd($_POST['password']);
		foreach($_POST['roles'] as $role_id) {
			$user->getRoles()->addRole((new RoleEntity())->setId($role_id));
		}
		if ( $userDAO->create($user) ) {
			http_response_code(201);
			printf(self::get_message("user_created"), $user->getId(), $user->getUsername());
		}
		else {
			http_response_code(500);
			print self::get_message("user_creating_error");
		}
			
		
	
	}
	
	/**
	 * Elimina un usuario concreto de la base de datos.
	 * @param array $vars Debe contener el elemento 'id' con el identificador del usuario
	 * @precondition El usuario debe existir o devolverá mensaje descriptivo con estado 404
	 * @poscondition El usuario es eliminado, devolviendo mensaje descriptivo y estado 200
	 */
	static function users_delete($vars) {
		/* @var UserEntity */
		$user = new UserEntity();
		$user->setId($vars['id']);
		/* @var UserDAO */
		$userDAO = new UserDAO();
		if ($userDAO->delete($user)) {
			http_response_code(200);
			printf(self::get_message("user_deleted"), $user->getId(), $user->getUsername());
		}
		else {
			http_response_code(404);
			print self::get_message("user_not_exists");
		}
	}
	

	/**
	 * Se actualiza la información de un usuario concreto
	 * @param array $vars Debe contener el elemento 'id' con el identificador del usuario
	 * @precondition El usuario debe existir o devolverá mensaje descriptivo con estado 404, en caso
	 * 					de que los datos sean incorrectos o que el servidor no haya podido procesar la información
	 * 					se devolverá mensaje descriptivo con estado 500
	 * @poscondition El usuario queda actualizado en la base de datos, devolviendo mensaje descriptivo y código 200
	 */
	static function users_update($vars) {
		/* @var UserEntity */
		$user = new UserEntity();
		$user->setId($vars['id']);
		/* @var UserDAO */
		$userDAO = new UserDAO();
		if( $userDAO->getById($user) !== false) {
			if(!empty($_POST['username'])) $user->setUsername($_POST['username']);
			if(!empty($_POST['password'])) $user->setPasswd($_POST['password']);
			
			$user->setRoles(new UserRolesEntity());
			foreach($_POST['roles'] as $role_id) {
				$user->getRoles()->addRole((new RoleEntity())->setId($role_id));
			}
			
			if ( $userDAO->modify($user) ) {
				http_response_code(200);
				printf(self::get_message("user_updated"), $user->getId(), $user->getUsername());
			}
			else {
				http_response_code(500);
				printf(self::get_message("user_updating_error"));
			}
		}
		else {
			http_response_code(404);
			print self::get_message("user_not_exists");
		}
	}
	
	/**
	 * Comprueba si la información de autentificación es correcta.
	 * 
	 * @precondition El usuario debe existir y su contraseña correcta o devolverá mensaje descriptivo con estado 401
	 * @poscondition Se registraran en sesion sus datos, excepto la contraseña y se le reenviará a la página de la que venía
	 * 				, en caso de haber accedido al login directamente se le enviará a su perfil
	 * @param string $username Nombre de usuario
	 * @param string $password Contraseña en claro
	 */
	static function login_logic($username = null, $password = null) {
		$user = new UserEntity();
		if(isset($username) && isset($password)) {
			$user->setUsername($username);
			$user->setPasswd($password);
		}
		else {
			$user->setUsername($_POST['username']);
			$user->setPasswd($_POST['password']);
		}
		
		$userDAO = new UserDAO();
		
		if ( $userDAO->exists($user) )
		{
			$user->setNullPasswd(); // Una vez se ha identificado, no queremos que la passwd este ni siquiera en RAM
			$userDAO->getByUserName($user);
			if(session_status() == PHP_SESSION_NONE)
				session_start();
			$_SESSION["user"] = serialize($user);
			session_commit();
			if(isset($_POST['request_uri']))
				header("Location: /page/".$_POST['request_uri']);
 			else
 				header("Location: /profile");
		}
		
		else
		{
			http_response_code(401); // unauthorized
			print self::get_message("login_invalid_auth");

			exit();
		}
	}
	
	/**
	 * Comprueba que el usuario de la sesión tiene el role correspondiente
	 * @param string $tag Nombre asociado al rolel, ejemplo: ADMIN
	 * 
	 * @return bool true si el usuario dispone del role, false en caso contrario
	 */
	static function login_hasRoleByTag($tag) {
		/** @var $user UserEntity */
		$user = self::getUserContext();
		if($user) {
			/** creating entity */
			$role = new RoleEntity();
			$role->setName($tag); // putting his name
			
			$roleDAO = new RoleDAO(); // looking for role
			$roleDAO->getByTag($role);
			return $user->getRoles()->hasRole($role);
		}
		return false;
	}
	
	/**
	 * Desloguea al usuario de la sesion
	 * @param string $tag Nombre asociado al rolel, ejemplo: ADMIN
	 *
	 * @return bool true si el usuario dispone del role, false en caso contrario
	 */
	static function login_logout() {
		if(self::login_is_logged()) {
			session_destroy();
			http_response_code(401); // forcing HTTP reauth
			header("Location: /");
		}
		else 
			print self::get_message("login_not_logged");
	}
	
	/**
	 * Obtiene el contexto del usuario.
	 * 
	 * @return \Eneas\Model\UserEntity Objeto del usuario, false en caso de que no hubiera ningún usuario en sesión
	 */
	static function getUserContext() {
		if(self::login_is_logged())
			return unserialize($_SESSION["user"]);
		return false;
	}
	
	/**
	 * Comprueba que el usuario está logueado
	 * 
	 * @return true en caso de estar logueado, false en caso contrario
	 */
	static function login_is_logged() {
		 if (session_status() == PHP_SESSION_NONE)
		 	session_start();
		return 	isset($_SESSION["user"]);
		
	}
	
	/**
	 * Devuelve mensaje en la lengua preferida que el cliente solicita
	 * @param string $token Token con el código del mensaje que se solicita
	 * @return string Mensaje en la lengua preferida
	 */
	static function get_message($token) {
		$lang = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) : "en";
		require $_SERVER['DOCUMENT_ROOT'] . "/theme/i18n.php";
		switch ($lang){
			case "es":
			case "en":
				return $messages[$token][$lang];
				break;
			default:
				return $messages[$token]["en"];
				break;
		}
	}
}
