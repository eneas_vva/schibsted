<?php namespace Eneas\Db;
use \PDO;

class DBConnection {

	protected static $instance_production;
	protected static $instance_testing;
	
	private function __construct($db_uri, $testing=false) {

		try {
			$instance = new PDO($db_uri);
			$instance->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}
		catch (PDOException $e) {
			echo "Connection Error: " . $e->getMessage();
		}
		
		if($testing)
			self::$instance_testing = $instance;
		else
			self::$instance_production = $instance;

	}
	public static function getInstance() {

		if (!self::$instance_production) {
			new DBConnection('sqlite:src/Db/production.sqlite');
		}

		return self::$instance_production;
	}
	/** @return PDO */
	public static function getInstanceTesting() {
	
		if (!self::$instance_testing) {
			new DBConnection('sqlite:src/Db/testing.sqlite', 1);
		}
	
		return self::$instance_testing;
	}
}

?>