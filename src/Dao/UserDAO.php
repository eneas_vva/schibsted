<?php namespace Eneas\Dao;

use Eneas\Model\UserEntity;
use \PDO;
use Eneas\Model\UserRolesEntity;
use Eneas\Db\DBConnection;

class UserDAO {
	/** @var PDO */
	private $datasource;
	
	function __construct(PDO $datasource = null) {
		$this->datasource = ($datasource) ? $datasource : DBConnection::getInstance();
	}
	

	/**
	 * Obtiene todas las instancias de usuario
	 */
	function getAll() {
		 $users = $this->datasource->query('SELECT id_user, username FROM `users`', PDO::FETCH_ASSOC);
		 $users_list = array();
		 foreach($users as $user)
		 {
		 	$user_model = new UserEntity();
		 	$user_model->setId($user['id_user']);
		 	$user_model->setUsername($user['username']);
		 	$usersRolesDAO = new UsersRolesDAO();
		 	$usersRolesDAO->getByUserId($user_model);
		 	
		 	array_push($users_list, $user_model);
		 }
		
		 return $users_list;
	}
	
	/** @return bool */
	function getById(UserEntity &$entity) {
		$stmt = $this->datasource->prepare('SELECT * FROM `users` WHERE id_user=:id_user');
		$stmt->execute(array(":id_user" => $entity->getId()));
		$user = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return ($user) ? $this->get($entity, $user) : false;
		
	}
	
	/** @return bool */
	function getByUserName(UserEntity &$entity) {
		$stmt = $this->datasource->prepare('SELECT * FROM `users` WHERE username=:username');
		$stmt->execute(array(":username" => $entity->getUsername()));
		$user = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return ($user) ? $this->get($entity, $user) : false;
	}
	
	function get(UserEntity &$entity, $user) {

		$entity->setId($user['id_user']);
		$entity->setUsername($user['username']);
		
		$usersRoles_model = new UsersRolesDAO();
		$usersRoles_model->getByUserId($entity);
	}
	
	/**
	 * Crea un nuevo usuario
	 * @param UserEntity $entity
	 */
	function create(UserEntity $entity) {
		$stmt = $this->datasource->prepare("INSERT INTO users (username, password) VALUES(?, ?)");
	
		if ( $stmt->execute(array ( $entity->getUsername(), $entity->getPasswd() ) ) )
		{
			$entity->setId( $this->datasource->lastInsertId() );
			$usersRolesDAO = new UsersRolesDAO();
			return $usersRolesDAO->create($entity);
				
		}
		else return false;
	}
	
	function delete(UserEntity &$entity) {
		$stmt = $this->datasource->prepare("DELETE FROM `users` WHERE id_user=:id_user");
		$stmt->execute(array(":id_user" => $entity->getId()));
		
		return (bool)$stmt->rowCount();
	}
	
	function modify(UserEntity &$entity) {
		$success = 0;
		$stmt = $this->datasource->prepare("UPDATE `users` SET username=:username, password=:password WHERE id_user=:id_user");
		$success = $stmt->execute(
				array(	":id_user" => $entity->getId(),
						":username" => $entity->getUsername(),
					  	":password" => $entity->getPasswd()
				));
		$usersRolesDAO = new UsersRolesDAO();
		$success = ($success) ? $usersRolesDAO->modify($entity) : 0;
		
		return $success;
	}
	
	function exists(UserEntity $entity) {
		$stmt = $this->datasource->query("SELECT COUNT(*) FROM `users` WHERE username=:username AND password=:password");
		$stmt->execute(
						array(
								":username" => $entity->getUsername(),
								":password" => $entity->getPasswd()
						));
		 
		return ($stmt->fetchColumn() == 1);
	}
}