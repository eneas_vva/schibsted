<?php namespace Eneas\Dao;

use \PDO;
use Eneas\Model\UserRolesEntity;
use Eneas\Model\RoleEntity;
use Eneas\Db\Singleton;
use Eneas\Db\DBConnection;

class RoleDAO {
	/** @var PDO */
	private $datasource;
	
	function __construct(PDO $datasource = null) {
		$this->datasource = ($datasource) ? $datasource : DBConnection::getInstance();
		
	}
	
	/**
	 * @return array
	 */
	function getAll() {
		$roles_db = $this->datasource->query('SELECT id_role, role_name FROM `roles`', PDO::FETCH_ASSOC);
		$roles_list = array();
		foreach($roles_db as $role) {
			$role_model = new RoleEntity();
			$role_model->setId($role['id_role'])->setName($role['role_name']);
			array_push($roles_list, $role_model);
		}
		
		return $roles_list;
	}
	
	function getByTag(RoleEntity &$role) {
		$role_db = $this->datasource->prepare('SELECT id_role FROM `roles` WHERE role_name=:role_name');
		$role_db->execute(array("role_name" => $role->getName()));
		
		$role->setId($role_db->fetchColumn());
	}
}
	