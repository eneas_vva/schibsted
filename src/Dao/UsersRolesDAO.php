<?php namespace Eneas\Dao;

use \PDO;
use Eneas\Model\UserEntity;
use Eneas\Model\RoleEntity;
use Eneas\Model\UserRolesEntity;
use Eneas\Db\DBConnection;

class UsersRolesDAO {
	/** @var PDO */
	private $datasource;
	
	function __construct(PDO $datasource = null) {
		$this->datasource = ($datasource) ? $datasource : DBConnection::getInstance();	
	}
	
	
	function getByUserId(UserEntity &$user) {
		$stmt = $this->datasource->prepare('SELECT id_role, role_name FROM `users_roles` NATURAL JOIN `roles` WHERE id_user=:id_user');
		$stmt->execute(array(":id_user"=>$user->getId()));
		
		return $this->getByUser($user, $stmt);
	}
	function getByUserName(UserEntity &$user) {
		$stmt = $this->datasource->prepare('SELECT id_role, role_name FROM `users_roles` NATURAL JOIN `roles` NATURAL JOIN `users` WHERE username=:username');
		$stmt->execute(array(":username"=>$user->getUsername()));
		
		return $this->getByUser($user, $stmt);
	}
	
	function getByUser(UserEntity &$user, \PDOStatement $roles) {
		$userRoles_model = new UserRolesEntity();
		foreach($roles as $role) {
			$role_model = new RoleEntity();
			$role_model->setId($role['id_role']);
			$role_model->setName($role['role_name']);
			
			$userRoles_model->addRole($role_model);
		}
		
		$user->setRoles($userRoles_model);
		
	}
	function create(UserEntity &$user) {
		/** @var $role RoleEntity */
		foreach ($user->getRoles()->getAll() as $role) {
			if (
				!	$this->datasource->prepare("INSERT INTO users_roles (id_user, id_role) VALUES(?,?)")->execute(array($user->getId(), $role->getId()))
				) return false;
		}
		
		return true;
	}
	function delete(UserEntity $user) {
		$stmt = $this->datasource->prepare("DELETE FROM `users_roles` WHERE id_user=:id_user");
		return $stmt->execute(array(":id_user" => $user->getId()));
		
	}
	
	function modify(UserEntity $user) {
		$success = $this->delete($user);
		$success = ($success) ? $this->create($user) : 0;
		return $success;
	}
	
}