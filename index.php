<?php
use Eneas\Controller\Controller;

require __DIR__ . '/vendor/autoload.php';

session_start();
Controller::check_activity();

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
	$r->addRoute('GET', '/users[/]', 'users_list');
	$r->addRoute('GET', '/users/new', 'users_new_form');
    $r->addRoute('POST', '/users', 'users_new');
    // {id} must be a number (\d+)
    $r->addRoute('GET', '/users/{id:\d+}', 'users_request');
    $r->addRoute('GET', '/users/{id:\d+}/update', 'users_update_form');
    $r->addRoute('PUT', '/users/{id:\d+}', 'users_update');
    $r->addRoute('DELETE', '/users/{id:\d+}', 'users_delete');
    
    // login
    $r->addRoute('POST', '/login[/]' , 'login_logic');
    $r->addRoute('GET', '/login[/{request_uri}]', 'login_form');
    $r->addRoute('GET', '/logout', 'login_logout');
    
    // profile
    $r->addRoute('GET', '/profile', 'login_profile');
    
    // getting pages
    $r->addRoute('GET', '/page/{id:\d+}', 'get_page');
    
    $r->addRoute('GET', '/', 'get_index');
    
});
// only admins can handle this functions
$protect_handlers = ['users_new_form', 'users_new', 'users_update', 'users_update_form', 'users_delete'];

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// _method will rewrite http method behaviour
if(isset($_REQUEST['_method'])) {
	$httpMethod = $_REQUEST['_method'];
}

// php doesn't have put or delete native support
if($httpMethod == "PUT" || $httpMethod == "DELETE")
	parse_str(file_get_contents('php://input'), $_POST);
	
// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        http_response_code(404);
        print Controller::get_message("http_not_found");
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        http_response_code(405);
        print Controller::get_message("http_method_disallowed");
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        //we check if you're requesting user api and you aren't logged in session method
        if(!Controller::login_is_logged() && substr($handler, 0, strlen("users")) === "users") { // if $handler starts with "users" ...       	
        	Controller::send_http_auth_request();
        }
        
        // check if is protected by ADMIN role
        if(array_search($handler, $protect_handlers) !== FALSE && !Controller::login_hasRoleByTag('ADMIN')) {
        	http_response_code(403); // not the best site, but it's okay for this target
        	print  Controller::get_message("login_admin_required");
        }
       	else 
       		Controller::$handler($vars);
        	
        break;
}



