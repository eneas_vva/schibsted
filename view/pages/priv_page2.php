<?php
use \Eneas\Controller\Controller;

if(isset($has_permission) && $has_permission=true):
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><?php printf("%s %d", Controller::get_message("content_title"), 2)?></title>
</head>
<body>
<?php printf("%s %s", Controller::get_message("content_welcome"), Controller::getUserContext()->getUsername()); ?> <a href='/logout'>logout</a>
<?php else: ?>
<p>Esta página solo puede ser accedida mediante el punto de acceso </p>
<?php endif; ?>
</body>
</html>