<?php 
use \Eneas\Controller\Controller;
if(!Controller::login_is_logged()) {
	header('Location: /login');
	exit;
}
	
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Profile</title>
</head>
<body>
<p><?php echo Controller::get_message("login_logged_as") ?> <?php echo Controller::getUserContext()->getUsername(); ?></p>
<a href='/logout'>logout</a>
</body>
</html>