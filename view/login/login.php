<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<body>
<form action="/login" method="POST">
	<?php if(isset($vars['request_uri'])): ?>
	<input type="hidden" name="request_uri" value="<?php echo $vars['request_uri'];?>" />
	<?php endif; ?>
	<div>
		<label for="username">username</label>
		<input type="text" name="username" />
	</div>
	<div>
		<label for="password">password</label>
		<input type="password" name="password" />
	</div>
	<input type="submit" value="login" />
</form>
</body>
</html>