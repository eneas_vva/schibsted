<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New user</title>
</head>
<body>
<form action="/users" method="POST">
	<div>
		<label for="username">Username</label>
		<input type="text" name="username" />
	</div>
	<div>
		<label for="password">Password</label>
		<input type="password" name="password" />
	</div>
	<div>
		<label for="roles">Roles separated with comma</label>
		<select name="roles[]" multiple>
		<?php
			$roleDao = new \Eneas\Dao\RoleDAO();
			foreach($roleDao->getAll() as $role)
				printf("<option value='%d'>%s</option>", $role->getId(), $role->getName());
			
		?>
		</select>
	</div>
	<div>
		<input type="submit" />
	</div>
</form>
</body>
</html>