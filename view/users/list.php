<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Users list</title>
</head>
<body>
<h1>Lista de usuarios</h1>
<table border="1">
	<tr>
		<th>ID</th>
		<th>Username</th>
		<th>Roles</th>
		<th colspan="2">Actions</th>
	</tr>
<?php
 	$userDao = new \Eneas\Dao\UserDAO();

	foreach($userDao->getAll() as $user) {
		$delete_link = sprintf("<form method='POST' action='/users/%d'><input type='hidden' name='_method' value='DELETE' /><input type='submit' value='delete' /></form>", $user->getId());
		$update_link = sprintf("<a href='/users/%d/update'>update</a>", $user->getId());
		printf("<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>", $user->getId(), $user->getUsername(), $user->getRoles(), $delete_link, $update_link);
	}
?>
</table>
<a href="/users/new">create a new user</a>
</body>
</html>