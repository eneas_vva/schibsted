<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New user</title>
</head>
<body>
<form action="/users/<?php echo $user->getId(); ?>" method="POST">
	<input type="hidden" name="_method" value="PUT">
	<div>
		<label for="username">Username</label>
		<input type="text" name="username" value="<?php echo $user->getUsername(); ?>" />
	</div>
	<div>
		<label for="password">Password</label>
		<input type="password" name="password" value="********" />
	</div>
	<div>
		<label for="roles">Roles separated with comma</label>
		<select name="roles[]" multiple>
		<?php
			$roleDao = new \Eneas\Dao\RoleDAO();
			foreach($roleDao->getAll() as $role) {
				$hasRole = ( $user->getRoles()->hasRole($role) ) ? "selected" : "";
				printf("<option %s value='%d'>%s</option>", $hasRole, $role->getId(), $role->getName());
			}
		?>
		</select>
	</div>
	<div>
		<input type="submit" value="update" />
	</div>
</form>
</body>
</html>